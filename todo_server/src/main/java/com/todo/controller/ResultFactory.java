package com.todo.controller;

/**
 * 工厂抽象类
 */
public abstract class ResultFactory {
    public abstract Result createOkResult(Object data,String msg);
    public abstract Result createFailResult(String msg);
}
