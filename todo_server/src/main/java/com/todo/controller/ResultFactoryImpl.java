package com.todo.controller;

/**
 * 工厂实现类
 * 实现了工厂抽象类的抽象方法，用于创建具体的 返回结果。
 */
public class ResultFactoryImpl extends ResultFactory{
    @Override
    public Result createOkResult(Object data, String msg) {
        return new Result(true,data,msg);
    }

    @Override
    public Result createFailResult(String msg) {
        return new Result(false,null,msg);
    }
}
